package com.silverorange.videoplayer

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.silverorange.videoplayer.Adapters.VideoAdapter


class MainActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView;
    lateinit var swipeRefreshLayout: SwipeRefreshLayout;
    lateinit var videoAdapter: VideoAdapter;
    lateinit var viewModel: ViewModel;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recycler_view);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh);

        val nonScrollingLinearLayoutManager = object : LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false) {
            override fun canScrollHorizontally(): Boolean {
                return true;
            }
        }
        recyclerView.layoutManager = nonScrollingLinearLayoutManager;
        recyclerView.setHasFixedSize(true)

        videoAdapter = VideoAdapter(baseContext, recyclerView)
        val snapHelper: SnapHelper = PagerSnapHelper()
        recyclerView.adapter = videoAdapter;
        snapHelper.attachToRecyclerView(recyclerView);

        viewModel = ViewModel(this.applicationContext, videoAdapter, swipeRefreshLayout);
        swipeRefreshLayout.isRefreshing = true;

        swipeRefreshLayout.setOnRefreshListener(OnRefreshListener {
            viewModel.updateData();
        })
        viewModel.updateData()
    }


}