package com.silverorange.videoplayer.Adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.silverorange.videoplayer.Model.Video;
import com.silverorange.videoplayer.R;

import java.util.ArrayList;

import io.noties.markwon.Markwon;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    ArrayList<Video> videos = new ArrayList<Video>();
    RecyclerView recyclerView;
    private Context context;
    public VideoAdapter(Context context, RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.context = context;
    }

    public void addItem(Video video) {
        videos.add(video);
    }

    public void clear() {
        videos.clear();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Video video = videos.get(position);

        holder.title.setText(video.getTitle());
        holder.author.setText(video.getAuthor().getName());
        holder.description.setText(video.getDescription());

        final Markwon markwon = Markwon.create(holder.description.getContext());
        markwon.setMarkdown(holder.description, video.getDescription());
        setVideoPlayer(holder, video, position);
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    public void setVideoPlayer(ViewHolder holder, Video video, int position) {
        Uri uri = Uri.parse(video.getHlsUrl());

        ExoPlayer player = new ExoPlayer.Builder(context).build();
        holder.videoView.setPlayer(player);
        MediaItem mediaItem = MediaItem.fromUri(uri);
        player.setMediaItem(mediaItem);
        holder.videoView.setUseController(false);
        player.prepare();

        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (player.isPlaying()) {
                    player.pause();
                    holder.play.setImageResource(R.drawable.pause);
                } else {
                    player.play();
                    holder.play.setImageResource(R.drawable.play);
                }
            }
        });

        holder.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position + 1 < videos.size()) {
                    recyclerView.smoothScrollToPosition(position + 1);
                }
            }
        });

        holder.previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position > 0) {
                    recyclerView.smoothScrollToPosition(position - 1);
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final TextView author;
        public final TextView description;

        public final StyledPlayerView videoView;

        public final ImageButton previous;
        public final ImageButton play;
        public final ImageButton next;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            author = (TextView) itemView.findViewById(R.id.author);
            description = (TextView) itemView.findViewById(R.id.description);

            videoView = (StyledPlayerView) itemView.findViewById(R.id.video_view);

            previous = (ImageButton) itemView.findViewById(R.id.previous_button);
            play = (ImageButton) itemView.findViewById(R.id.play_button);
            next = (ImageButton) itemView.findViewById(R.id.next_button);
        }
    }
}
