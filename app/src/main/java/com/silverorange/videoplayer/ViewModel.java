package com.silverorange.videoplayer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.silverorange.videoplayer.Adapters.VideoAdapter;
import com.silverorange.videoplayer.Model.Video;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ViewModel {
    private OkHttpClient httpClient = new OkHttpClient();
    private VideoAdapter adapter;
    private SwipeRefreshLayout refreshLayout;
    private Context context;

    public ViewModel (Context context, VideoAdapter adapter, SwipeRefreshLayout refreshLayout) {
        this.adapter = adapter;
        this.refreshLayout = refreshLayout;
        this.context = context;
    }

    public void updateData() {
        Request request = new Request.Builder()
                .url("http://10.0.2.2:4000/videos")
                .build();

        Call call = httpClient.newCall(request);
        call.enqueue(new Callback() {
            public void onResponse(Call call, Response response)
                    throws IOException {
                if (response.isSuccessful()) {
                    Type listType = new TypeToken<ArrayList<Video>>() {
                    }.getType();
                    List<Video> videos = new Gson().fromJson(response.body().string(), listType);

                    updateDataSet(videos);
                    //we want to stop the swipeRefreshListener, but this is done asynchronously
                    //we can use this little work around. Another option would be to use RxJava
                    Handler mainHandler = new Handler(Looper.getMainLooper());
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            refreshLayout.setRefreshing(false);
                        }
                    });
                }
            }

            public void onFailure(Call call, IOException e) {
                //show failure list item
                Handler mainHandler = new Handler(Looper.getMainLooper());
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(false);
                    }
                });
                e.printStackTrace();
            }
        });
    }

    public void updateDataSet(List<Video> response) {
        adapter.clear();
        //This should be enough to sort the list as needed, then we can reverse if needed.
        //unfortunately i was not able to figure out what this date format is
        //2018-12-14T21:09:00.000Z and thus was not able to get the comparator working

//        Collections.sort(response, new Comparator<Video>() {
//            @Override
//            public int compare(Video video, Video t1) {
//                Date a = video.getDateTime();
//                Date b = t1.getDateTime();
//                return a.compareTo(b);
//            }
//        });

        for (Video video : response) {
            adapter.addItem(video);
        }
    }
}
